﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab1_RoshanShah.Models
{
    /// <summary>
    /// Model class for the team entity
    /// </summary>
    public class Team
    {
        /// <summary>
        /// team id
        /// </summary>
        [Required]
        [Display(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// team name
        /// </summary>
        [Required]
        [Display(Name = "Team Name")]
        public string TeamName { get; set; }
       
        /// <summary>
        /// email of the team
        /// </summary>
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }


        /// <summary>
        /// established date of the team
        /// </summary>
        [Display(Name = "Established Date")]
        public DateTimeOffset EstablishedDate { get; set; }
    }
}
