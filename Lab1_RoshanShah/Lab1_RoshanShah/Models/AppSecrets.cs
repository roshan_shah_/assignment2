﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab1_RoshanShah.Models
{
    public class AppSecrets
    {
        public string AdminPassword { get; set; }
        public string MemberPassword { get; set; }
    }
}
